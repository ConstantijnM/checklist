import pprint
import xlsxwriter
import datetime
from benedict import benedict
from xlsxwriter.utility import xl_rowcol_to_cell

# Read yaml file
checklist = benedict.from_yaml('./checklist.yaml')

# Debug output: yaml content
pprint.pprint(checklist)


# Create new Excel workbook
workbook = xlsxwriter.Workbook('Checklist-formulier.xlsx')
workbook.set_size(1200, 1200)
workbook.set_properties({
    'title':    'Common Ground Checklist',
    'author':   'VNG Realisatie',
    'company':  'of Wolves',
    'created':  datetime.datetime.now(),
    'comments': 'https://gitlab.com/commonground/docs/checklist'})

# Prepare styles
# 000000 // Black
# 002d61 // Dark blue
# 004584 // Less dark blue
# 777777 // Grey
# ef7c36 // orange
# 029ed4 // light blue
# 00aaef // ligher blue
# ffffff // White

headerStyle = workbook.add_format({'bold': True, 'font': 'Calibri', 'size': 36, 'color': '#000000'})
subHeaderStyle = workbook.add_format({'bold': True, 'font': 'Calibri', 'size': 24, 'color': '#029ed4'})
subSubHeaderStyle = workbook.add_format({'bold': True, 'font': 'Calibri', 'size': 20, 'color': '#002d61'})
itemStyle = workbook.add_format({'bold': True, 'font': 'Calibri', 'size': 16, 'text_wrap': True})
questionStyle = workbook.add_format({'bold': True, 'font': 'Calibri', 'size': 14, 'text_wrap': True, 'align':'top'})
answerStyle = workbook.add_format({'font': 'Calibri', 'size': 14, 'text_wrap': True, 'align':'top', 'border': 1})

normalStyle = workbook.add_format({'font': 'Calibri', 'size': 16, 'color': '#000000', 'text_wrap': True})
orangeStyle = workbook.add_format({'bold': True, 'font': 'Calibri', 'size': 16, 'align':'top', 'color': '#ef7c36'})
greyStyle = workbook.add_format({'bold': True, 'font': 'Calibri', 'size': 16, 'align':'top', 'color': '#777777'})


checkboxStyle = workbook.add_format({'font': 'Calibri', 'size': 30, 'align':'center vcenter', 'border': 2, 'locked': True})

# Generate cover page (locked)
summarySheet = workbook.add_worksheet('Samenvatting')
summarySheet.set_column(1, 1, 40)
summarySheet.set_column(2, 2, 60)
summarySheet.write(1, 1, "Checklist Common Ground", headerStyle)

row = 3
col = 1

summarySheet.write(row, col, "CG Checklist", subHeaderStyle)

summarySheet.write_formula(row, col, "=Meetinformatie!B6", itemStyle)
summarySheet.write_formula(row, col+1, "=Meetinformatie!C6", itemStyle)
row += 1
summarySheet.write_formula(row, col, "=Meetinformatie!B7", itemStyle)
summarySheet.write_formula(row, col+1, "=Meetinformatie!C7", itemStyle)
row += 1
summarySheet.write_formula(row, col, "=Meetinformatie!B8", itemStyle)
summarySheet.write_formula(row, col+1, "=Meetinformatie!C8", itemStyle)
row += 2
summarySheet.write(row, col, "Totaalscore", itemStyle)
summarySheet.write_formula(row, col+1, '=COUNTIF(Beoordeling!B4:Beoordeling!B99,"X")', itemStyle)

summarySheet.protect()

# Generate sheet 1 - checklist (locked)
praktijkSheet = workbook.add_worksheet('Praktijkaspecten')
praktijkSheet.set_column(1, 1, 100)
praktijkSheet.write(1, 1, "Praktijkaspecten principes Common Ground", headerStyle)

row = 3
col = 1
for principle in checklist['principles']:
    praktijkSheet.write(row, col, principle['name'], subHeaderStyle)
    row += 1
    row += 1
    praktijkSheet.write(row, col, "Praktijk", subSubHeaderStyle)
    row += 1
    for practicalAspect in principle['practice']:
        praktijkSheet.write(row, col, practicalAspect, itemStyle)
        row += 1
    row += 1
    praktijkSheet.write(row, col, "Meetbaar maken", subSubHeaderStyle)
    row += 1
    for smartAspect in principle['smart']:
        praktijkSheet.write(row, col, smartAspect, itemStyle)
        row += 1
    row += 1

praktijkSheet.protect()
# praktijkSheet.activate()

# Generate sheet 2 - meting
measurementSheet = workbook.add_worksheet('Meetinformatie')
measurementSheet.set_column(1, 1, 40)
measurementSheet.set_column(2, 2, 60)
measurementSheet.write(1, 1, "Meetinformatie Common Ground software", headerStyle)

row = 3
col = 1
# Algemene vragen
measurementSheet.write(row, col, "Algemene vragen", subHeaderStyle)
row += 2
measurementSheet.write(row, col, "Naam softwareproduct", questionStyle)
measurementSheet.write(row, col + 1, "<naam>", answerStyle)
row += 1
measurementSheet.write(row, col, "Bron van meetgegevens:", questionStyle)
measurementSheet.write(row, col + 1, "<organisatie die deze sheet invult>", answerStyle)
row += 1
measurementSheet.write(row, col, "Datum meting", questionStyle)
measurementSheet.write(row, col + 1, "<datum en evt tijd>", answerStyle)
row += 1
measurementSheet.write(row, col, "Context meting", questionStyle)
measurementSheet.write(row, col + 1, "<ingevuld in de context van ...>", answerStyle)
row += 2

# Inhoudelijke vragen
measurementSheet.write(row, col, "Inhoudelijke vragen", subHeaderStyle)
row += 2
for item in checklist['items']:
    measurementSheet.write(row, col, item['smart'], questionStyle)
    measurementSheet.write(row, col + 1, item['smartTemplate'], answerStyle)
    measurementSheet.set_row(row, 40)
    row += 1

# measurementSheet.activate()


# Generate sheet 3 - normering (locked)
normSheet = workbook.add_worksheet('Normering')
normSheet.set_column(1, 1, 10)
normSheet.set_column(2, 2, 90)
normSheet.write(1, 1, "Normering voor Common Ground software", headerStyle)

row = 3
col = 1

for item in checklist['items']:
    normSheet.write(row, col, item['name'], subHeaderStyle)
    row += 1
    normSheet.write(row, col, "Praktijk", orangeStyle)
    normSheet.write(row, col + 1, item['practice'], normalStyle)
    row += 1
    normSheet.write(row, col, "Norm", itemStyle)
    normSheet.write(row, col + 1, item['norm'], normalStyle)
    row += 2

normSheet.protect()
# normSheet.activate()


# // Generate sheet 4 - beoordeling
assesmentSheet = workbook.add_worksheet('Beoordeling')
assesmentSheet.set_column(1, 1, 3.5)
assesmentSheet.set_column(2, 2, 40)
assesmentSheet.set_column(3, 3, 60)
assesmentSheet.write(1, 1, "Beoordeling Common Ground software", headerStyle)

row = 3
col = 1

for item in checklist['items']:
    assesmentCellString = xl_rowcol_to_cell(row+3, col+2)
    assesmentSheet.write_formula(row, col, '=IF('+assesmentCellString+'="ja","X","")', checkboxStyle)
    assesmentSheet.write(row, col + 1, item['name'], subHeaderStyle)
    row += 1
    assesmentSheet.write(row, col + 1, "Norm", itemStyle)
    assesmentSheet.write(row, col + 2, item['norm'], itemStyle)
    row += 1
    assesmentSheet.write(row, col + 1, item['smart'], itemStyle)
    measurementCellString = xl_rowcol_to_cell(11+(row//5), 2)
    assesmentSheet.write_formula(row, col + 2, '=Meetinformatie!'+measurementCellString, itemStyle)
    row += 1
    assesmentSheet.write(row, col + 1, "Voldoet aan norm", itemStyle)
    assesmentSheet.write(row, col + 2, "ja/nee", orangeStyle)
    row += 2

# assesmentSheet.activate()

workbook.close()